package com.bw.gmall.realtime.common.util;

import com.bw.gmall.realtime.common.constant.Constant;
import com.ververica.cdc.connectors.mysql.source.MySqlSource;
import com.ververica.cdc.connectors.mysql.table.StartupOptions;
import com.ververica.cdc.debezium.JsonDebeziumDeserializationSchema;
import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class FlinkSourceUtil {
    public static KafkaSource<String> getKafkaSource(String topic,String groupId) {
//                                                          组id   kafka主题
        return KafkaSource.<String>builder()
                .setBootstrapServers(Constant.KAFKA_BROKERS)
                .setGroupId(groupId)
                .setTopics(topic)
                .setStartingOffsets(OffsetsInitializer.earliest())
//              .setStartingOffsets(OffsetsInitializer.latest())
                .setValueOnlyDeserializer(new DeserializationSchema<String>() {
                    @Override     // 从最新的开始消费
                    public String deserialize(byte[] message) throws IOException {
                        if (message != null) {
                            return new String(message, StandardCharsets.UTF_8); // 将字节数组转换为字符串
                        }
                        return null;
                    }
                    @Override
                    public boolean isEndOfStream(String nextElement) { //  消费完之后是否结束
                        return false;
                    }
                    @Override
                    public TypeInformation<String> getProducedType() { //  返回类型
                        return Types.STRING;
                    }
                })
                .build(); //  构建kafkaSource
    }
    /**
     * 读取CDC
     *
     * @param processDataBase
     * @param processDimTableName
     * @return
     */
    public static MySqlSource<String> getMysqlSource(String processDataBase, String processDimTableName) {
        Properties props = new Properties();
        props.setProperty("useSSL", "false");
        props.setProperty("allowPublicKeyRetrieval", "true");
        return MySqlSource.<String>builder()
                .jdbcProperties(props)
                .hostname(Constant.MYSQL_HOST)
                .port(Constant.MYSQL_PORT)
                .databaseList(Constant.PROCESS_DATABASE) // monitor all tables under inventory database
                .username(Constant.DORIS_USERNAME)
                .password(Constant.MYSQL_PASSWORD)
                .tableList(processDataBase + "." + processDimTableName)
                .deserializer(new JsonDebeziumDeserializationSchema()) // converts SourceRecord to String
                .startupOptions(StartupOptions.initial())
                .build();

    }

}