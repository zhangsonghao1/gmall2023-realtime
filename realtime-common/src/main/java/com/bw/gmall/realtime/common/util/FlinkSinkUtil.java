package com.bw.gmall.realtime.common.util;

import com.bw.gmall.realtime.common.constant.Constant;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.connector.base.DeliveryGuarantee;
import org.apache.flink.connector.kafka.sink.KafkaRecordSerializationSchema;
import org.apache.flink.connector.kafka.sink.KafkaSink;

public class FlinkSinkUtil {

    public static KafkaSink<String> getKafkaSink(String topicName){
       return  KafkaSink.<String>builder()
                .setBootstrapServers(Constant.KAFKA_BROKERS)
                .setRecordSerializer(KafkaRecordSerializationSchema.builder()
                        .setTopic(topicName)
                        .setValueSerializationSchema(new SimpleStringSchema())
                        .build()
                )
               .setDeliveryGuarantee(DeliveryGuarantee.EXACTLY_ONCE)
               .setTransactionalIdPrefix("bw-" + topicName + System.currentTimeMillis())
               .setProperty("transaction.timeout.ms", 15 * 60 * 1000 + "")
               .build();

    }
}
